var express = require('express');
var router = express.Router();
var xmlconvert = require('xml-js');
var axios = require('axios');

/* GET home page. */
router.get('/:id?',async function(req, res, next) {
    var data = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cus="http://siebel.com/CustomUI">
   <soapenv:Header/>
   <soapenv:Body>
      <cus:ASWMemberInboundMemberQuery_Input>
         <cus:RequestXML><![CDATA[<?xml version="1.0" encoding="utf-8"?>
<SiebelMessage>
  <Header>
    <Action>QUERY</Action>
    <Env>Web</Env>
    <BUID>MBHK</BUID>
    <Sent>26/07/2021</Sent>
    <From>Website@FTRHK</From>
    <To>Siebel@Europe</To>
  </Header>
  <Body>
    <Customer>
      <MemberNumber>${req.params['id']}</MemberNumber>
    </Customer>
  </Body>
</SiebelMessage>]]></cus:RequestXML>
      </cus:ASWMemberInboundMemberQuery_Input>
   </soapenv:Body>
</soapenv:Envelope>`
    var config = {
        method: 'post',
        url: 'http://cdc-api.aswatson.net/eai_anon_enu/start.swe?SWEExtSource=AnonWebService&SWEExtCmd=Execute&WSSOAP=1',
        headers: {
            'Content-Type': 'text/xml;charset=UTF-8',
            'SOAPAction': '"document/http://siebel.com/CustomUI:ASWMemberInboundMemberQuery"'
        },
        data : data
    };
    var resp = (await axios(config)).data;
    resp = resp.replace(/&lt;/gm,'<').replace(/&gt;/gm,'>').match(/(?<=\<ns1\:ResponseXML\>).*\<SiebelMessage\>.*\<\/SiebelMessage\>/gms)
    resp = xmlconvert.xml2json(resp,{compact:true,space:4})
    res.send(resp);
});







module.exports = router;
